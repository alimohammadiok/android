package com.test.androidprojesiv2;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

public class SoruSorActivity extends Activity 
{
	ProgressDialog dialogKutusu;
	private long dersID;
	private String dersAD;
	private long kullaniciID;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_soru_sor);
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		dersID = getIntent().getLongExtra("dersID", 0);
		dersAD = getIntent().getStringExtra("dersAD");
		kullaniciID = getIntent().getLongExtra("kullaniciID", 0);
		
		((TextView) findViewById(R.id.DERSADI)).setText(dersAD);
	}
	
	public void soruEkle(View view) 
	{
		String sorulanSoru = ((EditText) findViewById(R.id.text_SORULANSORU)).getText().toString();
		new asynSoruEkle().execute(sorulanSoru);
	}
	
	public class asynSoruEkle extends AsyncTask<String, Void, Boolean>
	{	
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			
			dialogKutusu = new ProgressDialog(SoruSorActivity.this);
			dialogKutusu.setMessage("Lütfen Bekleyiniz...");
			dialogKutusu.setCancelable(false);
			dialogKutusu.show();
		}

		@Override
		protected Boolean doInBackground(String... params) 
		{
			
			final String METHOD_NAME = "soruEkle";
			final String NAMESPACE = "http://192.168.111.1/";
			final String SOAP_ACTION = "http://192.168.111.1/soruEkle";
			final String URL = "http://192.168.111.1/Service1.asmx";
			
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			
			request.addProperty("dersID", dersID);
			request.addProperty("soruMETNI", params[0]);

		    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		    envelope.dotNet = true;
		    envelope.setOutputSoapObject(request);
		    
		    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		    try {
				androidHttpTransport.call(SOAP_ACTION, envelope);
			} catch (HttpResponseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}
		    
		    SoapPrimitive response = null;
			try {
				response = (SoapPrimitive) envelope.getResponse();
			} catch (SoapFault e) {
				e.printStackTrace();
			}
		    
		    Boolean sonucBoolean;
		    sonucBoolean = Boolean.parseBoolean(response.toString());
		    
		    return sonucBoolean;
		}
		
		@Override
		protected void onPostExecute(Boolean result)
		{
			super.onPostExecute(result);
			dialogKutusu.dismiss();
			
			if (result=true) 
			{
				Toast.makeText(getApplicationContext(), "Sorunuz Başarılı Bir Şekilde Eklendi", Toast.LENGTH_SHORT).show();
			} 
			else
			{
				Toast.makeText(getApplicationContext(), "İşleminiz Başarısız Oldu", Toast.LENGTH_SHORT).show();
			}
			
			Intent intent = new Intent(SoruSorActivity.this, SorulanSorularActivity.class);
			intent.putExtra("dersID", dersID);
			intent.putExtra("dersAD", dersAD);
			intent.putExtra("kullaniciID", kullaniciID);
			startActivity(intent);
		}
	}

	public void geriDonSorulanSorular(View view)
	{
		Intent intent = new Intent(SoruSorActivity.this, SorulanSorularActivity.class);
		intent.putExtra("dersID", dersID);
		intent.putExtra("dersAD", dersAD);
		intent.putExtra("kullaniciID", kullaniciID);
		startActivity(intent);
	}
}