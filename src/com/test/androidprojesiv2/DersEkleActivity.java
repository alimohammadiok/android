package com.test.androidprojesiv2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

public class DersEkleActivity extends Activity 
{
	ProgressDialog dialogKutusu;
	private long kullaniciID;
	View arg1View;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ders_ekle);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();

		kullaniciID = getIntent().getLongExtra("kullaniciID", 0);
		new asyncDersListesiAl().execute(kullaniciID);
	}
	
	public class asyncDersListesiAl extends AsyncTask<Long, Void, List<DersListesi_Bilgisi>>
	{
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			dialogKutusu = new ProgressDialog(DersEkleActivity.this);
			dialogKutusu.setMessage("Lütfen Bekleyiniz...");
			dialogKutusu.setCancelable(false);
			dialogKutusu.show();
		}
		
		@Override
		protected List<DersListesi_Bilgisi> doInBackground(Long... params) 
		{
			return DersListesi_Fonksiyonlari.kullanicininSecmedigiDersler(params[0]);
		}
		
		@Override
		protected void onPostExecute(List<DersListesi_Bilgisi> result) 
		{
			super.onPostExecute(result);
			dialogKutusu.dismiss();

			dersEklemeListesiniOlustur(result);
		}
	}
	
	public void dersEklemeListesiniOlustur(List<DersListesi_Bilgisi> dersListesi)
	{
		if (dersListesi == null)
		{
			Toast.makeText(getApplicationContext(), "Bütün Dersler Zaten Seçilmiş", Toast.LENGTH_LONG).show();
		}
		else
		{
			ListView listView_Ders_Listesi = (ListView) findViewById(R.id.list_Ders_Listesi);
			
			final List<DersListesi_Bilgisi> dersler = new ArrayList<DersListesi_Bilgisi>();
			for (DersListesi_Bilgisi geciciDersListesi : dersListesi) 
			{
				dersler.add(new DersListesi_Bilgisi(geciciDersListesi.getDersID(), geciciDersListesi.getDersAD()));
			}
			
			DersEkle_ListAdapter myListAdapter = new DersEkle_ListAdapter(this, dersler);
			listView_Ders_Listesi.setAdapter(myListAdapter);
			
			listView_Ders_Listesi.setOnItemClickListener(new OnItemClickListener() 
			{
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
				{
					arg1View = arg1;
					
					new asynDersEkle().execute(kullaniciID, dersler.get(position).getDersID());
				}
			});
		}
	}
	
	public class asynDersEkle extends AsyncTask<Long, Void, Boolean>
	{	
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			
			dialogKutusu = new ProgressDialog(DersEkleActivity.this);
			dialogKutusu.setMessage("Lütfen Bekleyiniz...");
			dialogKutusu.setCancelable(false);
			dialogKutusu.show();
		}

		@Override
		protected Boolean doInBackground(Long... params) 
		{
			
			final String METHOD_NAME = "dersEkle";
			final String NAMESPACE = "http://192.168.111.1/";
			final String SOAP_ACTION = "http://192.168.111.1/dersEkle";
			final String URL = "http://192.168.111.1/Service1.asmx";
			
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			
			request.addProperty("kullaniciID", params[0]);
			request.addProperty("dersID", params[1]);

		    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		    envelope.dotNet = true;
		    envelope.setOutputSoapObject(request);
		    
		    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		    try {
				androidHttpTransport.call(SOAP_ACTION, envelope);
			} catch (HttpResponseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}
		    
		    SoapPrimitive response = null;
			try {
				response = (SoapPrimitive) envelope.getResponse();
			} catch (SoapFault e) {
				e.printStackTrace();
			}
		    
		    Boolean sonucBoolean;
		    sonucBoolean = Boolean.parseBoolean(response.toString());
		    
		    return sonucBoolean;
		}
		
		@Override
		protected void onPostExecute(Boolean result)
		{
			super.onPostExecute(result);
			dialogKutusu.dismiss();
			
			if (result=true) 
			{
				Toast.makeText(getApplicationContext(), "Ders Eklendi", Toast.LENGTH_SHORT).show();
				
				arg1View.setClickable(true);
				arg1View.setBackgroundColor(0xFF99CC00);
			} 
			else
			{
				Toast.makeText(getApplicationContext(), "İşleminiz Başarısız Oldu", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	public void geriDonDersListesi(View view)
	{
		Intent intent = new Intent(DersEkleActivity.this, DersListesiActivity.class);
		intent.putExtra("kullaniciID", kullaniciID);
		startActivity(intent);
	}
}