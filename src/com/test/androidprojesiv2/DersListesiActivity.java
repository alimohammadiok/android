package com.test.androidprojesiv2;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

public class DersListesiActivity extends Activity 
{
	ProgressDialog dialogKutusu;
	private long kullaniciID;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ders_listesi);
	}

	@Override
	protected void onResume()
	{
		super.onResume();

		kullaniciID = getIntent().getLongExtra("kullaniciID", 0);
		new asyncDersListesiAl().execute(kullaniciID);
	}

	public class asyncDersListesiAl extends AsyncTask<Long, Void, List<DersListesi_Bilgisi>>
	{
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			dialogKutusu = new ProgressDialog(DersListesiActivity.this);
			dialogKutusu.setMessage("Lütfen Bekleyiniz...");
			dialogKutusu.setCancelable(false);
			dialogKutusu.show();
		}
		
		@Override
		protected List<DersListesi_Bilgisi> doInBackground(Long... params) 
		{
			return DersListesi_Fonksiyonlari.kullanicininDersListesi(params[0]);
		}
		
		@Override
		protected void onPostExecute(List<DersListesi_Bilgisi> result) 
		{
			super.onPostExecute(result);
			dialogKutusu.dismiss();

			dersListesiniOlustur(result);
		}
	}
	
	public void dersListesiniOlustur(List<DersListesi_Bilgisi> dersListesi)
	{
		if (dersListesi == null)
		{
			Toast.makeText(getApplicationContext(), "Lütfen Ders Seçimini Yapınız", Toast.LENGTH_LONG).show();
		}
		else
		{
			GridView gridView_Ders_Listesi = (GridView) findViewById(R.id.gridView_Ders_Listesi);

			final List<DersListesi_Bilgisi> dersler = new ArrayList<DersListesi_Bilgisi>();
			for (DersListesi_Bilgisi geciciDersListesi : dersListesi) 
			{
				dersler.add(new DersListesi_Bilgisi(geciciDersListesi.getDersID(), geciciDersListesi.getDersAD()));
			}
			
			DersListesi_GridAdapter myGridAdapter = new DersListesi_GridAdapter(this, dersler);
			gridView_Ders_Listesi.setAdapter(myGridAdapter);
			
			gridView_Ders_Listesi.setOnItemClickListener(new OnItemClickListener() 
			{
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
				{
					Intent intent = new Intent(DersListesiActivity.this, SorulanSorularActivity.class);
					intent.putExtra("dersID", dersler.get(position).getDersID());
					intent.putExtra("dersAD", dersler.get(position).getDersAD());
					intent.putExtra("kullaniciID", kullaniciID);
					startActivity(intent);
				}
			});
		}
	}
	
	public void dersEkleSayfasiniAc(View view)
	{
		Intent intent = new Intent(DersListesiActivity.this, DersEkleActivity.class);
		intent.putExtra("kullaniciID", kullaniciID);
		startActivity(intent);
	}
	
	public void dersSilSayfasiniAc(View view)
	{
		Intent intent = new Intent(DersListesiActivity.this, DersSilActivity.class);
		intent.putExtra("kullaniciID", kullaniciID);
		startActivity(intent);
	}
	
	public void geriDonGiris(View view) 
	{
		startActivity(new Intent(DersListesiActivity.this, GirisActivity.class));
	}
	
}
