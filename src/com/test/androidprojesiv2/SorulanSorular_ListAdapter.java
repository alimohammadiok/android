package com.test.androidprojesiv2;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SorulanSorular_ListAdapter extends BaseAdapter
{
	private LayoutInflater inflater;
	private List<SorulanSorular_Bilgisi> soruListesi;
	
	public SorulanSorular_ListAdapter(Activity activity, List<SorulanSorular_Bilgisi> sorular)
	{
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		soruListesi = sorular;
	}
	
	@Override
	public int getCount() 
	{
		return soruListesi.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return soruListesi.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View view = convertView;
		
		if (convertView == null)
		{
			view = inflater.inflate(R.layout.item_sorulan_sorular, null);
		}
		
		TextView textView = (TextView) view.findViewById(R.id.textView_Sorulan_Sorular);
		
		SorulanSorular_Bilgisi soru = soruListesi.get(position);
		textView.setText(soru.getSoruMETNI());
		
		return view;
	}
}