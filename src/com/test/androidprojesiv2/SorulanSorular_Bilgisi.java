package com.test.androidprojesiv2;

import java.io.Serializable;

public class SorulanSorular_Bilgisi implements Serializable
{
	private static final long serialVersionUID = 1L;
	private long soruID;
	private String soruMETNI;
	
	public SorulanSorular_Bilgisi()
	{
		super();
	}
	
	public SorulanSorular_Bilgisi(long soruID, String soruMETNI)
	{
		this.soruID = soruID;
		this.soruMETNI = soruMETNI;
	}
	
	public void setSoruID(long soruID)
	{
		this.soruID = soruID;
	}
	
	public void setSoruMETNI(String soruMETNI)
	{
		this.soruMETNI = soruMETNI;
	}
	
	public long getSoruID()
	{
		return this.soruID;
	}
	
	public String getSoruMETNI() 
	{
		return this.soruMETNI;
	}

    @Override
    public String toString() 
    {
      return "SorulanSorular_Bilgisi [soruID=" + soruID + ", soruMETNI=" + soruMETNI +  "]";
    }
}