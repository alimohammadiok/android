package com.test.androidprojesiv2;

import java.io.Serializable;

public class YapilanYorumlar_Bilgisi implements Serializable
{
	private static final long serialVersionUID = 1L;
	private long yorumID;
	private String yorumMETNI;
	
	public YapilanYorumlar_Bilgisi()
	{
		super();
	}
	
	public YapilanYorumlar_Bilgisi(long yorumID, String yorumMETNI)
	{
		this.yorumID = yorumID;
		this.yorumMETNI = yorumMETNI;
	}
	
	public void setYorumID(long yorumID)
	{
		this.yorumID = yorumID;
	}
	
	public void setYorumMETNI(String yorumMETNI)
	{
		this.yorumMETNI = yorumMETNI;
	}
	
	public long getYorumID()
	{
		return this.yorumID;
	}
	
	public String getYorumMETNI() 
	{
		return this.yorumMETNI;
	}

    @Override
    public String toString() 
    {
      return "YapilanYorumlar_Bilgisi [yorumID=" + yorumID + ", yorumMETNI=" + yorumMETNI +  "]";
    }
}