package com.test.androidprojesiv2;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DersSil_ListAdapter extends BaseAdapter
{
	private LayoutInflater inflater;
	private List<DersListesi_Bilgisi> dersListesi;
	
	public DersSil_ListAdapter(Activity activity, List<DersListesi_Bilgisi> dersler)
	{
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		dersListesi = dersler;
	}
	
	@Override
	public int getCount() 
	{
		return dersListesi.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return dersListesi.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View view = convertView;
		
		if (convertView == null)
		{
			view = inflater.inflate(R.layout.item_ders_sil, null);
		}
		
		TextView textView = (TextView) view.findViewById(R.id.textView_DERS_SILME_LISTESI);
		
		DersListesi_Bilgisi ders = dersListesi.get(position);
		textView.setText(ders.getDersAD());
		
		return view;
	}
}