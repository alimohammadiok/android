package com.test.androidprojesiv2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class DersListesi_Fonksiyonlari 
{
	private final static String NAMESPACE = "http://192.168.111.1/";
	private final static String URL = "http://192.168.111.1/Service1.asmx";
	
	public static List<DersListesi_Bilgisi> kullanicininDersListesi(long kullaniciID)
	{
		final String METHOD_NAME = "secilenDerslerinListesi";
		final String SOAP_ACTION = "http://192.168.111.1/secilenDerslerinListesi";
		
		List<DersListesi_Bilgisi> liste = null;
		
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	    request.addProperty("kullaniciID", kullaniciID);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		
		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);
		} catch (HttpResponseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}

		try 
		{
			SoapObject response = (SoapObject) envelope.getResponse();
			
			if (response.toString().equals("anyType{}") || response == null) 
			{
				liste = null;
			}
			else
			{
				liste = new ArrayList<DersListesi_Bilgisi>();
				
				int dersSayisi = response.getPropertyCount();
				for (int i = 0; i < dersSayisi; i++) 
				{
					SoapObject geciciSoapObject  = (SoapObject) response.getProperty(i);
					DersListesi_Bilgisi geciciDersListesi = new DersListesi_Bilgisi();
					
					if (geciciSoapObject.hasProperty("dersID")) 
					{
						geciciDersListesi.setDersID(Long.parseLong(geciciSoapObject.getPropertyAsString("dersID")));
					}
					
					if (geciciSoapObject.hasProperty("dersAD")) 
					{
						geciciDersListesi.setDersAD(geciciSoapObject.getPropertyAsString("dersAD"));
					}
					
					liste.add(geciciDersListesi);
				}
			}
			
		} catch (SoapFault e) {
			e.printStackTrace();
		}

		return liste;
	}

	public static List<DersListesi_Bilgisi> kullanicininSecmedigiDersler(long kullaniciID)
	{
		final String METHOD_NAME = "secilmeyenDerslerinListesi";
		final String SOAP_ACTION = "http://192.168.111.1/secilmeyenDerslerinListesi";
		
		List<DersListesi_Bilgisi> liste = null;
		
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	    request.addProperty("kullaniciID", kullaniciID);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		
		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);
		} catch (HttpResponseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}

		try 
		{
			SoapObject response = (SoapObject) envelope.getResponse();
			
			if (response.toString().equals("anyType{}") || response == null) 
			{
				liste = null;
			}
			else
			{
				liste = new ArrayList<DersListesi_Bilgisi>();
				
				int dersSayisi = response.getPropertyCount();
				for (int i = 0; i < dersSayisi; i++) 
				{
					SoapObject geciciSoapObject  = (SoapObject) response.getProperty(i);
					DersListesi_Bilgisi geciciDersListesi = new DersListesi_Bilgisi();
					
					if (geciciSoapObject.hasProperty("dersID")) 
					{
						geciciDersListesi.setDersID(Long.parseLong(geciciSoapObject.getPropertyAsString("dersID")));
					}
					
					if (geciciSoapObject.hasProperty("dersAD")) 
					{
						geciciDersListesi.setDersAD(geciciSoapObject.getPropertyAsString("dersAD"));
					}
					
					liste.add(geciciDersListesi);
				}
			}
			
		} catch (SoapFault e) {
			e.printStackTrace();
		}

		return liste;
	}
}