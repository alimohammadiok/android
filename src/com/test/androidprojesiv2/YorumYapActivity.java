package com.test.androidprojesiv2;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

public class YorumYapActivity extends Activity 
{
	ProgressDialog dialogKutusu;
	private long soruID;
	private String soruMETNI;
	private String dersAD;
	private long dersID;
	private long kullaniciID;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_yorum_yap);
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		soruID = getIntent().getLongExtra("soruID", 0);
		soruMETNI = getIntent().getStringExtra("soruMETNI");
		dersAD = getIntent().getStringExtra("dersAD");
		dersID = getIntent().getLongExtra("dersID", 0);
		kullaniciID = getIntent().getLongExtra("kullaniciID", 0);
		
		TextView textSORUMETNI = (TextView) findViewById(R.id.textView_SORUMETNI);
		textSORUMETNI.setText(soruMETNI);
	}
	
	public void yorumEkle(View view) 
	{
		String yapilanYorum = ((EditText) findViewById(R.id.editText_YAPILANYORUM)).getText().toString();
		new asynYorumEkle().execute(yapilanYorum);
	}
	
	public class asynYorumEkle extends AsyncTask<String, Void, Boolean>
	{	
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			
			dialogKutusu = new ProgressDialog(YorumYapActivity.this);
			dialogKutusu.setMessage("Lütfen Bekleyiniz...");
			dialogKutusu.setCancelable(false);
			dialogKutusu.show();
		}

		@Override
		protected Boolean doInBackground(String... params) 
		{
			final String METHOD_NAME = "yorumEkle";
			final String NAMESPACE = "http://192.168.111.1/";
			final String SOAP_ACTION = "http://192.168.111.1/yorumEkle";
			final String URL = "http://192.168.111.1/Service1.asmx";
			
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			
			request.addProperty("soruID", soruID);
			request.addProperty("yorumMETNI", params[0]);

		    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		    envelope.dotNet = true;
		    envelope.setOutputSoapObject(request);
		    
		    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		    try {
				androidHttpTransport.call(SOAP_ACTION, envelope);
			} catch (HttpResponseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}
		    
		    SoapPrimitive response = null;
			try {
				response = (SoapPrimitive) envelope.getResponse();
			} catch (SoapFault e) {
				e.printStackTrace();
			}
		    
		    Boolean sonucBoolean;
		    sonucBoolean = Boolean.parseBoolean(response.toString());
		    
		    return sonucBoolean;
		}
		
		@Override
		protected void onPostExecute(Boolean result)
		{
			super.onPostExecute(result);
			dialogKutusu.dismiss();
			
			if (result=true) 
			{
				Toast.makeText(getApplicationContext(), "Yorumunuz Başarılı Bir Şekilde Eklendi", Toast.LENGTH_SHORT).show();
			} 
			else
			{
				Toast.makeText(getApplicationContext(), "İşleminiz Başarısız Oldu", Toast.LENGTH_SHORT).show();
			}
			
			Intent intent = new Intent(YorumYapActivity.this, YapilanYorumlarActivity.class);
			intent.putExtra("soruID", soruID);
			intent.putExtra("soruMETNI", soruMETNI);
			intent.putExtra("dersAD", dersAD);
			intent.putExtra("dersID", dersID);
			intent.putExtra("kullaniciID", kullaniciID);
			startActivity(intent);
		}
	}
	
	public void geriDonYapilanYorumlar(View view)
	{
		Intent intent = new Intent(YorumYapActivity.this, YapilanYorumlarActivity.class);
		intent.putExtra("soruID", soruID);
		intent.putExtra("soruMETNI", soruMETNI);
		intent.putExtra("dersAD", dersAD);
		intent.putExtra("dersID", dersID);
		intent.putExtra("kullaniciID", kullaniciID);
		startActivity(intent);
	}
}