package com.test.androidprojesiv2;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

public class GirisActivity extends Activity 
{
	private int sayac = 3;
	
	EditText kullaniciAdiEditText;
	EditText sifreEditText;
	ProgressDialog dialogKutusu;
	Button girisButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_giris);
		
		Button kayitButton = (Button) findViewById(R.id.btn_kayit_yap);
		kayitButton.setOnClickListener(new OnClickListener() 
		{
			@Override
			public void onClick(View v) 
			{
				startActivity(new Intent(getApplicationContext(), KullaniciEkleActivity.class));
			}
		});
	}
	
	public void girisBilgisiAl(View view)
	{
		kullaniciAdiEditText = (EditText) findViewById(R.id.edt_kullaniciadi);
		sifreEditText = (EditText) findViewById(R.id.edt_sifre);
		girisButton = (Button) findViewById(R.id.btn_giris_yap);
		
		new asynGirisBilgisiAl().execute(kullaniciAdiEditText.getText().toString(), sifreEditText.getText().toString());
	}

	public class asynGirisBilgisiAl extends AsyncTask<String, Void, Giris_Bilgisi>
	{	
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			
			dialogKutusu = new ProgressDialog(GirisActivity.this);
			dialogKutusu.setTitle("Sunucuya Bağlanıyor");
			dialogKutusu.setMessage("Lütfen Bekleyiniz...");
			dialogKutusu.setIcon(R.drawable.ic_launcher);
			dialogKutusu.setCancelable(false);
			dialogKutusu.show();
		}

		@Override
		protected Giris_Bilgisi doInBackground(String... params) 
		{
			return Giris_Fonksiyonlari.girisYap(params[0], params[1]);
		}
		
		@Override
		protected void onPostExecute(Giris_Bilgisi result)
		{
			super.onPostExecute(result);

			dialogKutusu.dismiss();
			girisYap(result);
		}
	}
	
	public void girisYap(Giris_Bilgisi sonuc)
	{
		if (sonuc.dogruKullaniciMi() == true) 
		{
			Intent intent = new Intent(GirisActivity.this, DersListesiActivity.class);
			intent.putExtra("kullaniciID", sonuc.getKullaniciID());
			startActivity(intent);
		}
		else 
		{
			sayac--;
			if (sayac == 0) 
			{
				girisButton.setEnabled(false);
			}
			Toast.makeText(this, "Yanlış Kimlik Bilgisi", Toast.LENGTH_LONG).show();
		}
	}
}
