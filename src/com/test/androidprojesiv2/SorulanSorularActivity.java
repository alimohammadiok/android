package com.test.androidprojesiv2;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

public class SorulanSorularActivity extends Activity 
{
	ProgressDialog dialogKutusu;
	private long dersID;
	private String dersAD;
	private long kullaniciID;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sorulan_sorular);
	}
	
	@Override
	protected void onResume()
	{
		super.onResume();
		
		dersID = getIntent().getLongExtra("dersID", 0);
		dersAD = getIntent().getStringExtra("dersAD");
		kullaniciID = getIntent().getLongExtra("kullaniciID", 0);
		
		TextView dersAdiTextView = (TextView) findViewById(R.id.textView_Sorularin_Ders_Adi);
		dersAdiTextView.setText(dersAD);
		
		new asyncSoruListesiAl().execute(dersID);
	}
	
	public class asyncSoruListesiAl extends AsyncTask<Long, Void, List<SorulanSorular_Bilgisi>>
	{
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			dialogKutusu = new ProgressDialog(SorulanSorularActivity.this);
			dialogKutusu.setMessage("Lütfen Bekleyiniz...");
			dialogKutusu.setCancelable(false);
			dialogKutusu.show();
		}
		
		@Override
		protected List<SorulanSorular_Bilgisi> doInBackground(Long... params) 
		{
			return SorulanSorular_Fonksiyonlari.dersinSorulari(params[0]);
		}
		
		@Override
		protected void onPostExecute(List<SorulanSorular_Bilgisi> result) 
		{
			super.onPostExecute(result);
			dialogKutusu.dismiss();

			soruListesiniOlustur(result);
		}
	}
	
	public void soruListesiniOlustur(List<SorulanSorular_Bilgisi> soruListesi)
	{
		if (soruListesi == null)
		{
			Toast.makeText(getApplicationContext(), "Hiç Soru Sorulmamış", Toast.LENGTH_LONG).show();
		}
		else
		{
			ListView listView_Soru_Listesi = (ListView) findViewById(R.id.listView_Sorulan_Sorular);
			
			final List<SorulanSorular_Bilgisi> sorular = new ArrayList<SorulanSorular_Bilgisi>();
			
			for (SorulanSorular_Bilgisi geciciSoruListesi : soruListesi) 
			{
				sorular.add(new SorulanSorular_Bilgisi(geciciSoruListesi.getSoruID(), geciciSoruListesi.getSoruMETNI()));
			}
			
			SorulanSorular_ListAdapter myListAdapter = new SorulanSorular_ListAdapter(this, sorular);
			listView_Soru_Listesi.setAdapter(myListAdapter);
			
			listView_Soru_Listesi.setOnItemClickListener(new OnItemClickListener() 
			{
				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
				{
					Intent intent = new Intent(SorulanSorularActivity.this, YapilanYorumlarActivity.class);
					intent.putExtra("soruID", sorular.get(position).getSoruID());
					intent.putExtra("soruMETNI", sorular.get(position).getSoruMETNI());
					intent.putExtra("dersAD", dersAD);
					intent.putExtra("dersID", dersID);
					intent.putExtra("kullaniciID", kullaniciID);
					startActivity(intent);
				}
			});
		}
	}
	
	public void soruSorSayfasiniAc(View view)
	{
		Intent intent = new Intent(SorulanSorularActivity.this, SoruSorActivity.class);
		intent.putExtra("dersID", dersID);
		intent.putExtra("dersAD", dersAD);
		intent.putExtra("kullaniciID", kullaniciID);
		startActivity(intent);
	}
	
	public void geriDonDersListesi(View view)
	{
		Intent intent = new Intent(SorulanSorularActivity.this, DersListesiActivity.class);
		intent.putExtra("kullaniciID", kullaniciID);
		startActivity(intent);
	}
}