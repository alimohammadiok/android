package com.test.androidprojesiv2;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class YapilanYorumlar_ListAdapter extends BaseAdapter
{
	private LayoutInflater inflater;
	private List<YapilanYorumlar_Bilgisi> yorumListesi;
	
	public YapilanYorumlar_ListAdapter(Activity activity, List<YapilanYorumlar_Bilgisi> yorumlar)
	{
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		yorumListesi = yorumlar;
	}
	
	@Override
	public int getCount() 
	{
		return yorumListesi.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return yorumListesi.get(position);
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		View view = convertView;
		
		if (convertView == null)
		{
			view = inflater.inflate(R.layout.item_yapilan_yorumlar, null);
		}
		
		TextView textView = (TextView) view.findViewById(R.id.textView_Yapilan_Yorumlar);
		
		YapilanYorumlar_Bilgisi yorum = yorumListesi.get(position);
		textView.setText(yorum.getYorumMETNI());
		
		view.setClickable(true);
		
		return view;
	}
}