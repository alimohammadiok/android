package com.test.androidprojesiv2;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

public class KullaniciEkleActivity extends Activity 
{
	ProgressDialog dialogKutusu;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_kullanici_ekle);
	}
	
	public void kullaniciEkle(View view)
	{
		String ad = ((EditText) findViewById(R.id.AD)).getText().toString();
		String soyad = ((EditText) findViewById(R.id.SOYAD)).getText().toString();
		String kullaniciAdi = ((EditText) findViewById(R.id.KULLANICIADI)).getText().toString();
		String sifre = ((EditText) findViewById(R.id.SIFRE)).getText().toString();
		String email = ((EditText) findViewById(R.id.EMAIL)).getText().toString();
		
		new asynKullaniciEkle().execute(ad, soyad, kullaniciAdi, sifre, email);
	}
	
	public class asynKullaniciEkle extends AsyncTask<String, Void, Boolean>
	{	
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			
			dialogKutusu = new ProgressDialog(KullaniciEkleActivity.this);
			dialogKutusu.setMessage("Lütfen Bekleyiniz...");
			dialogKutusu.setCancelable(false);
			dialogKutusu.show();
		}

		@Override
		protected Boolean doInBackground(String... params) 
		{
			final String METHOD_NAME = "kullaniciEkle";
			final String NAMESPACE = "http://192.168.111.1/";
			final String SOAP_ACTION = "http://192.168.111.1/kullaniciEkle";
			final String URL = "http://192.168.111.1/Service1.asmx";
			
			SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
			
			request.addProperty("ad", params[0]);
			request.addProperty("soyad", params[1]);
		    request.addProperty("kullaniciAdi", params[2]);
		    request.addProperty("sifre", params[3]);
		    request.addProperty("email", params[4]);
		    
		    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		    envelope.dotNet = true;
		    envelope.setOutputSoapObject(request);
		    
		    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		    try {
				androidHttpTransport.call(SOAP_ACTION, envelope);
			} catch (HttpResponseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (XmlPullParserException e) {
				e.printStackTrace();
			}
		    
		    SoapPrimitive response = null;
			try {
				response = (SoapPrimitive) envelope.getResponse();
			} catch (SoapFault e) {
				e.printStackTrace();
			}
		    
		    Boolean sonucBoolean;
		    sonucBoolean = Boolean.parseBoolean(response.toString());
		    
		    return sonucBoolean;
		}
		
		@Override
		protected void onPostExecute(Boolean result)
		{
			super.onPostExecute(result);
			dialogKutusu.dismiss();
			
			if (result=true) 
			{
				Toast.makeText(getApplicationContext(), "Kullanıcı Başarılı Bir Şekilde Eklendi", Toast.LENGTH_SHORT).show();
				startActivity(new Intent(KullaniciEkleActivity.this, GirisActivity.class));
			} 
			else
			{
				Toast.makeText(getApplicationContext(), "İşleminiz Başarısız Oldu", Toast.LENGTH_SHORT).show();
			}
		}
	}
}