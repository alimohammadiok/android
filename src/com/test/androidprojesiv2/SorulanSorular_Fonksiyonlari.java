package com.test.androidprojesiv2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class SorulanSorular_Fonksiyonlari 
{
	private final static String METHOD_NAME = "derseAitSorular";
	private final static String NAMESPACE = "http://192.168.111.1/";
	private final static String SOAP_ACTION = "http://192.168.111.1/derseAitSorular";
	private final static String URL = "http://192.168.111.1/Service1.asmx";
	
	public static List<SorulanSorular_Bilgisi> dersinSorulari(long dersID)
	{
		List<SorulanSorular_Bilgisi> liste = null;
		
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	    request.addProperty("dersID", dersID);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		
		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);
		} catch (HttpResponseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}

		try 
		{
			SoapObject response = (SoapObject) envelope.getResponse();
			
			if (response.toString().equals("anyType{}") || response == null) 
			{
				liste = null;
			}
			else
			{
				liste = new ArrayList<SorulanSorular_Bilgisi>();
				
				int soruSayisi = response.getPropertyCount();
				for (int i = 0; i < soruSayisi; i++) 
				{
					SoapObject geciciSoapObject  = (SoapObject) response.getProperty(i);
					SorulanSorular_Bilgisi geciciSoruListesi = new SorulanSorular_Bilgisi();
					
					if (geciciSoapObject.hasProperty("soruID")) 
					{
						geciciSoruListesi.setSoruID(Long.parseLong(geciciSoapObject.getPropertyAsString("soruID")));
					}
					
					if (geciciSoapObject.hasProperty("soruMETNI")) 
					{
						geciciSoruListesi.setSoruMETNI(geciciSoapObject.getPropertyAsString("soruMETNI"));
					}
					
					liste.add(geciciSoruListesi);
				}
			}
			
		} catch (SoapFault e) {
			e.printStackTrace();
		}

		return liste;
	}
}