package com.test.androidprojesiv2;

import java.io.IOException;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class Giris_Fonksiyonlari 
{
	private final static String METHOD_NAME = "login";
	private final static String NAMESPACE = "http://192.168.111.1/";
	private final static String SOAP_ACTION = "http://192.168.111.1/login";
	private final static String URL = "http://192.168.111.1/Service1.asmx";
	
	public static Giris_Bilgisi girisYap(String kullaniciAdi, String sifre)
	{
		Giris_Bilgisi object_Giris_Bilgisi = new Giris_Bilgisi();
		
	    SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	    request.addProperty("kullaniciAdi", kullaniciAdi);
	    request.addProperty("sifre", sifre);
	    SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
	    envelope.dotNet = true;
	    envelope.setOutputSoapObject(request);
	    HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
	    
	    try {
			androidHttpTransport.call(SOAP_ACTION, envelope);
		} catch (HttpResponseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}
	    
	    try {
			SoapObject response = (SoapObject) envelope.getResponse();
			
			if (response.hasProperty("giris")) {
				if (response.getPropertyAsString("giris") == null) {
					object_Giris_Bilgisi.setDogruKullanici(false);
				} 
				else {
					object_Giris_Bilgisi.setDogruKullanici(Boolean.parseBoolean(response.getPropertyAsString("giris")));
				}
			}

			if (response.hasProperty("kullaniciId")) {
				if (response.getPropertyAsString("kullaniciId") == null) {
					object_Giris_Bilgisi.setKullaniciID(0);
				} else {
					object_Giris_Bilgisi.setKullaniciID(Long.parseLong(response.getPropertyAsString("kullaniciId")));
				}
			}
		} catch (SoapFault e) {
			e.printStackTrace();
		}
	    
	    return object_Giris_Bilgisi;
	}
}