package com.test.androidprojesiv2;

import java.io.Serializable;

public class DersListesi_Bilgisi implements Serializable
{
	private static final long serialVersionUID = 1L;
	private long dersID;
	private String dersAD;
	
	public DersListesi_Bilgisi()
	{
		super();
	}
	
	public DersListesi_Bilgisi(long dersID, String dersAD)
	{
		this.dersID = dersID;
		this.dersAD = dersAD;
	}
	
	public void setDersID(long dersID)
	{
		this.dersID = dersID;
	}
	
	public void setDersAD(String dersAD)
	{
		this.dersAD = dersAD;
	}
	
	public long getDersID()
	{
		return this.dersID;
	}
	
	public String getDersAD() 
	{
		return this.dersAD;
	}

    @Override
    public String toString() 
    {
      return "DersListesi_Bilgisi [dersID=" + dersID + ", dersAD=" + dersAD +  "]";
    }
}