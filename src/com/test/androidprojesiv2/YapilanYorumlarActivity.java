package com.test.androidprojesiv2;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

public class YapilanYorumlarActivity extends Activity 
{
	ProgressDialog dialogKutusu;
	
	private long dersID;
	private String dersAD;
	
	private long soruID;
	private String soruMETNI;
	
	private long kullaniciID;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_yapilan_yorumlar);
	}
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		
		dersAD = getIntent().getStringExtra("dersAD");
		soruID = getIntent().getLongExtra("soruID", 0);
		soruMETNI = getIntent().getStringExtra("soruMETNI");
		
		dersID = getIntent().getLongExtra("dersID", 0);
		kullaniciID = getIntent().getLongExtra("kullaniciID", 0);
		
		TextView textViewDersAdi = (TextView) findViewById(R.id.textView_Yorumlarin_Ders_Adi);
		textViewDersAdi.setText(dersAD);
		
		TextView textViewSoruMetni = (TextView) findViewById(R.id.textView_Soru_Metni);
		textViewSoruMetni.setText(soruMETNI);
		
		new asyncYorumListesiAl().execute(soruID);
	}

	public class asyncYorumListesiAl extends AsyncTask<Long, Void, List<YapilanYorumlar_Bilgisi>>
	{
		@Override
		protected void onPreExecute() 
		{
			super.onPreExecute();
			dialogKutusu = new ProgressDialog(YapilanYorumlarActivity.this);
			dialogKutusu.setMessage("Lütfen Bekleyiniz...");
			dialogKutusu.setCancelable(false);
			dialogKutusu.show();
		}
		
		@Override
		protected List<YapilanYorumlar_Bilgisi> doInBackground(Long... params) 
		{
			return YapilanYorumlar_Fonksiyonlari.sorununYorumlari(params[0]);
		}
		
		@Override
		protected void onPostExecute(List<YapilanYorumlar_Bilgisi> result) 
		{
			super.onPostExecute(result);
			dialogKutusu.dismiss();

			yorumListesiniOlustur(result);
		}
	}
	
	public void yorumListesiniOlustur(List<YapilanYorumlar_Bilgisi> yorumListesi)
	{
		if (yorumListesi == null)
		{
			Toast.makeText(getApplicationContext(), "Hiç Yorum Yapılmamış", Toast.LENGTH_LONG).show();
		}
		else
		{
			ListView listView_Yorum_Listesi = (ListView) findViewById(R.id.listView_Yapilan_Yorumlar);
			
			final List<YapilanYorumlar_Bilgisi> yorumlar = new ArrayList<YapilanYorumlar_Bilgisi>();
			
			for (YapilanYorumlar_Bilgisi geciciYorumListesi : yorumListesi) 
			{
				yorumlar.add(new YapilanYorumlar_Bilgisi(geciciYorumListesi.getYorumID(), geciciYorumListesi.getYorumMETNI()));
			}
			
			YapilanYorumlar_ListAdapter myListAdapter = new YapilanYorumlar_ListAdapter(this, yorumlar);
			
			listView_Yorum_Listesi.setAdapter(myListAdapter);
		}
	}
	
	public void yorumYapSayfasiniAc(View view)
	{
		Intent intent = new Intent(YapilanYorumlarActivity.this, YorumYapActivity.class);
		intent.putExtra("soruID", soruID);
		intent.putExtra("soruMETNI", soruMETNI);
		intent.putExtra("dersAD", dersAD);
		intent.putExtra("dersID", dersID);
		intent.putExtra("kullaniciID", kullaniciID);
		startActivity(intent);
	}
	
	public void geriDonSorulanSorular(View view)
	{
		Intent intent = new Intent(YapilanYorumlarActivity.this, SorulanSorularActivity.class);
		intent.putExtra("dersID", dersID);
		intent.putExtra("dersAD", dersAD);
		intent.putExtra("kullaniciID", kullaniciID);
		startActivity(intent);
	}
}