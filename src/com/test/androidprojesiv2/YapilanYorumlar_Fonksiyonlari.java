package com.test.androidprojesiv2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpResponseException;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

public class YapilanYorumlar_Fonksiyonlari 
{
	private final static String METHOD_NAME = "yapilanYorumlar";
	private final static String NAMESPACE = "http://192.168.111.1/";
	private final static String SOAP_ACTION = "http://192.168.111.1/yapilanYorumlar";
	private final static String URL = "http://192.168.111.1/Service1.asmx";
	
	public static List<YapilanYorumlar_Bilgisi> sorununYorumlari(long soruID)
	{
		List<YapilanYorumlar_Bilgisi> liste = null;
		
		SoapObject request = new SoapObject(NAMESPACE, METHOD_NAME);
	    request.addProperty("soruID", soruID);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = true;
		envelope.setOutputSoapObject(request);
		
		HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
		
		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);
		} catch (HttpResponseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (XmlPullParserException e) {
			e.printStackTrace();
		}

		try 
		{
			SoapObject response = (SoapObject) envelope.getResponse();
			
			if (response.toString().equals("anyType{}") || response == null) 
			{
				liste = null;
			}
			else
			{
				liste = new ArrayList<YapilanYorumlar_Bilgisi>();
				
				int yorumSayisi = response.getPropertyCount();
				for (int i = 0; i < yorumSayisi; i++) 
				{
					SoapObject geciciSoapObject  = (SoapObject) response.getProperty(i);
					YapilanYorumlar_Bilgisi geciciYorumListesi = new YapilanYorumlar_Bilgisi();
					
					if (geciciSoapObject.hasProperty("yorumID")) 
					{
						geciciYorumListesi.setYorumID(Long.parseLong(geciciSoapObject.getPropertyAsString("yorumID")));
					}
					
					if (geciciSoapObject.hasProperty("yorumMETNI")) 
					{
						geciciYorumListesi.setYorumMETNI(geciciSoapObject.getPropertyAsString("yorumMETNI"));
					}
					
					liste.add(geciciYorumListesi);
				}
			}
			
		} catch (SoapFault e) {
			e.printStackTrace();
		}

		return liste;
	}
}