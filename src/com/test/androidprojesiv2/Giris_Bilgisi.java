package com.test.androidprojesiv2;

public class Giris_Bilgisi
{
    private boolean dogruKullanici;
    private long kullaniciID;
    
    public Giris_Bilgisi()
    {
    	super();
    }
    
    public Giris_Bilgisi(boolean dogruKullanici, long kullaniciID)
    {
    	this.dogruKullanici = dogruKullanici;
    	this.kullaniciID = kullaniciID;
    }

    public void setDogruKullanici(boolean dogruKullanici)
    {
    	this.dogruKullanici = dogruKullanici;
    }
    
    public void setKullaniciID(long kullaniciID)
    {
    	this.kullaniciID = kullaniciID;
    }
    
    public boolean dogruKullaniciMi()
    {
    	return this.dogruKullanici;
    }

    public long getKullaniciID()
    {
    	return kullaniciID;
    }

    @Override
    public String toString() 
    {
      return "Login Bilgisi [dogruKullanici=" + dogruKullanici + ", kullaniciID=" + kullaniciID +  "]";
    }
}